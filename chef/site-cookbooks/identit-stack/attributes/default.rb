node.override['rvm']['default_ruby']  = "ruby-2.2.2"
node.override['rvm']['user_installs'] =
[
  { 'user'          => 'vagrant',
    'default_ruby'  => 'ruby-2.2.2',
    'rubies'        => ['2.2.2']
  }
]

node.override['rvm']['gems'] = {
  'ruby-2.2.2' => [
    { 'name'    => 'bundler' },
    { 'name'    => 'rails' }
  ]
}
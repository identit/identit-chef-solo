name             'identit-stack'
maintainer       'YOUR_COMPANY_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures identit-stack'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'git', '~> 4.3'
depends 'mysql', '~> 6.0'
depends 'nodejs', '~> 2.4.4'
depends 'build-essential', '~> 3.2.0'
depends 'imagemagick', '~> 0.2.3'
depends 'sqlite', '~> 1.1.3'
depends 'rvm'
#
# Cookbook Name:: helloworld
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

git_client 'default' do
  action :install
end

package 'nginx' do
  action :install
end
service 'nginx' do
  action [ :enable, :start ]
end

mysql_client 'default' do
  action :create
end

include_recipe "nodejs::nodejs_from_package"

include_recipe 'build-essential::default'

include_recipe "imagemagick::default"
include_recipe "imagemagick::devel"

include_recipe "sqlite::default"

include_recipe "rvm::user"

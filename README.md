# chef-rails-stack

Chef cookbook for efimeral servers setup using ubuntu.

## Details

Utilizar chef-client con la opción de local mode o chef solo

A instalar(cookbook/recipe):

* Cliente de mysql -- DONE
* Nginx web server -- DONE
* Git  -------------- DONE
* NodeJS  ----------- DONE
* Compiladores GCC/build essentials - DONE
* Imagemagick ------- DONE
* SQLite ------------ DONE
* RVM con Ruby 2.2.2 -- DONE
* bundler, rails gems - DONE

* Loggly client [setup](https://www.loggly.com/docs/rails-logs/)
* Run a test to find what is missing! using the NF App. Use vagrant or virtual machine

## Development

remember to run `librarian-chef install` before anything. Is like bundle install.


In the files path (solo.rb)
**For chef solo**
sudo chef-solo -c solo.rb -j solo.json

## References

* https://supermarket.chef.io
* https://chefiseasy.com/2014/02/24/chapter-01/comment-page-1/#comment-358
* http://www.davidmataro.com/my-first-approach-working-with-chef-local-mode
* https://launchschool.com/blog/chef-basics-for-rails-developers
* http://gettingstartedwithchef.com/first-steps-with-chef.html